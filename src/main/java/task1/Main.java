package task1;

/*
Create functional interface with method that accepts three int
values and return int value. Create lambda functions (as
variables in main method) what implements this interface:

First lambda returns max value
Second – average

Invoke thous lambdas.
 */
public class Main {

  public static void main(String[] args) {

    Max max = (a, b, c) -> Math.max(a, Math.max(b, c));
    System.out.println(max.getMax(5, 7, 98));

    Average average = (a, b, c) -> (a + b + c) / 3;
    System.out.println(average.getAverage(2, 2, 1));
  }
}
