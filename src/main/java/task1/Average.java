package task1;

@FunctionalInterface
public interface Average {

  int getAverage(int a, int b, int c);
}
