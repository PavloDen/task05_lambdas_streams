package task1;

@FunctionalInterface
public interface Max {

  int getMax(int a, int b, int c);
}
