package task3;

import java.util.Comparator;
import java.util.List;

public class Calculator {

  public Integer getMin(List<Integer> list) {
    return list.stream().min(Comparator.comparing(Integer::valueOf)).orElse(null);
  }

  public Integer getMax(List<Integer> list) {
    return list.stream().max(Comparator.comparing(Integer::valueOf)).orElseGet(null);
  }

  public Integer sum(List<Integer> list) {
    return list.stream().mapToInt(Integer::intValue).sum();
  }

  public Integer sumWithReduce(List<Integer> list) {
    return list.stream().reduce((left, right) -> left + right).orElseGet(null);
  }

  public Double getAverage(List<Integer> list) {
    return list.stream().mapToInt(Integer::intValue).average().orElse(0.0);
  }

  public Long getCountMoreThanAverage(List<Integer> list, Double average) {
    return list.stream().filter(i -> i > average).count();
  }
}
