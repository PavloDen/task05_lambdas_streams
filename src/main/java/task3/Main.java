package task3;

import java.util.List;

/*
Create a few methods that returns list (or array) of random integers. Methods should use streams API
and should be implemented using different Streams generators.
        Count average, min, max, sum of list values. Try to count sum using both reduce and sum Stream methods
        Count number of values that are bigger than average
 */
public class Main {

  public static void main(String[] args) {
    ConsoleListener consoleListener = new ConsoleListener();
    List<Integer> list = consoleListener.createData();
    System.out.println("List of integer " + list);

    Calculator calculator = new Calculator();
    System.out.println("Min element " + calculator.getMin(list));
    System.out.println("Max element " + calculator.getMax(list));
    System.out.println("Sum of elements " + calculator.sum(list));
    System.out.println("Sum of elements " + calculator.sumWithReduce(list));
    System.out.println("Average of elements " + calculator.getAverage(list));
    System.out.println(
        "Count number of values that are bigger than average "
            + calculator.getCountMoreThanAverage(list, calculator.getAverage(list)));
  }
}
