package task3;

import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ConsoleListener {

  public Integer getInteger() {
    Scanner keyboard = new Scanner(System.in);
    while (!keyboard.hasNextInt()) {
      System.out.println("   pls enter integer");
      keyboard.next();
    }
    return keyboard.nextInt();
  }

  public Integer getPositiveInteger() {
    Integer result;
    do {
      result = this.getInteger();
      if (result < 0) {
        System.out.println("   pls enter positive integer");
      }
    } while (result < 0);
    return result;
  }

  public List<Integer> createData() {
    System.out.println("Enter Low Range for Integer element ");
    Integer minElement = this.getInteger();
    System.out.println("Enter High Range for Integer element ");
    Integer maxElement = this.getInteger();
    System.out.println("Enter array length ");
    Integer arrLength = this.getPositiveInteger();

    List<Integer> list =
        new Random()
            .ints(arrLength, minElement, maxElement + 1)
            .boxed()
            .collect(Collectors.toList());
    return list;
  }
}
