package task2;

public class CommandImpl implements Command {
    @Override
    public String doCommand(String arg) {
        return arg.toUpperCase();
    }
}
