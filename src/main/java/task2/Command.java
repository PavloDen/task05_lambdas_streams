package task2;

@FunctionalInterface

public interface Command {

  String doCommand(String arg);

}
