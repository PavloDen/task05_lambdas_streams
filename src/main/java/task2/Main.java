package task2;

import java.util.Scanner;

/*
Implement pattern Command. Each command has its name (with which it is invoked)
and one string argument. You should implement 4 commands with next ways:
command as lambda function, as method reference, as anonymous class, as object
of command class. User enters command name and argument into console,
your app invokes corresponding command.
 */
public class Main {

  public static void main(String[] args) {

    String inputCommand;
    Scanner scanner = new Scanner(System.in);
    do {
      System.out.println("Enter commands : com1, com2, com3, com4");
      System.out.println("  For exit press Q");
      inputCommand = scanner.next().toUpperCase();
      switch (inputCommand) {

          // with lambda expression
        case "COM1":
          {
            Command command1 =
                (input) -> {
                  return input.toUpperCase();
                };
            System.out.println(" You enter com1 with arg = " + command1.doCommand(getArgument()));
            break;
          }

          // with method reference
        case "COM2":
          {
            Command command2 = (String::toUpperCase);
            System.out.println(" You enter com2 with arg = " + command2.doCommand(getArgument()));
            break;
          }

          // with anonymous class
        case "COM3":
          {
            Command command3 =
                new Command() {
                  @Override
                  public String doCommand(String arg) {
                    return arg.toUpperCase();
                  }
                };
            System.out.println(" You enter com3 with arg = " + command3.doCommand(getArgument()));
            break;
          }

          //  as object of command class
        case "COM4":
          {
            CommandImpl command4 = new CommandImpl();
            System.out.println(" You enter com3 with arg = " + command4.doCommand(getArgument()));
            break;
          }
      }
      if (!inputCommand.equals("COM1")
          && !inputCommand.equals("COM2")
          && !inputCommand.equals("COM3")
          && !inputCommand.equals("COM4")
          && !inputCommand.equals("Q")) {
        System.out.println("Enter correct command");
      }

    } while (!inputCommand.equals("Q"));
  }

  private static String getArgument() {
    System.out.println("Enter arg ");
    Scanner scanner = new Scanner(System.in);
    return scanner.next();
  };
}
