package task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleListener {

  public List<String> createText() {
    List<String> list = new ArrayList<>();
    System.out.println("Type text (Enter empty line for ending)");

    Scanner keyboard = new Scanner(System.in);
    String str;
    do {
      str = keyboard.nextLine();
      if (!str.isEmpty()) {
        list.add(str);
      }
    } while (!str.isEmpty());
    return list;
  }
}
