package task4;

import java.util.List;

public class Main {

  /*
  Create application. User enters some number of text lines (stop reading text when user enters empty line).
  Application returns:
  - Number of unique words
  - Sorted list of all unique words
  - Word count. Occurrence number of each word in the text (e.g. text “a s a” -> a-2 s-1 ). Use grouping collector.
  - Occurrence number of each symbol except upper case characters
   */
  public static void main(String[] args) {
    //
    ConsoleListener consoleListener = new ConsoleListener();
    List<String> list = consoleListener.createText();
    TextProcessor textProcessor = new TextProcessor(list);
    System.out.println(" Text = " + list);
    System.out.println("Number of unique words " + textProcessor.countUniqueWords());
    System.out.println("Sorted list of all unique words " + textProcessor.getSortedUniqueWords());
    System.out.println("Word count " + textProcessor.getWordsCount());
    System.out.println("Chars count " + textProcessor.getCharsOccurrence());
  }
}
