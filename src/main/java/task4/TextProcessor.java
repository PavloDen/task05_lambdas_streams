package task4;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class TextProcessor {
  private List<String> list;
  private Set<String> uniqueWords;

  public TextProcessor(List<String> list) {
    this.list = list;
    this.uniqueWords = new HashSet<>();
  }

  public long countUniqueWords() {
    findUniqueWords();
    return uniqueWords.size();
  }

  public List<String> getSortedUniqueWords() {
    findUniqueWords();
    List<String> sortedUniqueWords = new ArrayList<>(uniqueWords);
    return sortedUniqueWords.stream().sorted().collect(Collectors.toList());
  }

  public Map<String, Integer> getWordsCount() {
    List<String> wordsList = new ArrayList<>();
    list.stream()
        .map(str -> str.split("\\W+"))
        .forEach(
            str -> {
              List<String> words = Arrays.asList(str);
              wordsList.addAll(words);
            });
    Map<String, Integer> wordsCount =
        wordsList.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
    return wordsCount;
  }

  public Map<String, Integer> getCharsOccurrence() {
    List<String> charsList = new ArrayList<>();
    list.stream()
        .map(str -> str.replaceAll("[\\sA-Z]+", ""))
        .map(str -> str.split(""))
        .forEach(
            str -> {
              List<String> someChars = Arrays.asList(str);
              charsList.addAll(someChars);
            });
    Map<String, Integer> charsCount =
        charsList.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
    return charsCount;
  }

  private void findUniqueWords() {
    list.stream()
        .map(str -> str.split("\\W+"))
        .forEach(
            str -> {
              List<String> words = Arrays.asList(str);
              uniqueWords.addAll(words);
            });
  }
}
